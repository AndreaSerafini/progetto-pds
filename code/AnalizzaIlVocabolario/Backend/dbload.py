from django.db.models import Sum
from AnalizzaIlVocabolario.models import Contiene, Utenti, Testi, Parole


def carica_diz_freq(titolo):
    """
    Funzione che dato un titolo, fa una query al database e restituisce
    una lista con il suo dizionario delle frequenze e il suo autore.

    :param titolo: titolo del testo da cercare nel database.
    :return: una lista [dizionario delle frequenze, autore del testo]
    """
    diz_freq = dict()

    for relazione in Contiene.objects.filter(titolo=titolo).select_related('parola'):
        diz_freq[relazione.parola.parola] = relazione.freq

    autore = Testi.objects.filter(titolo=titolo).select_related('autore')[0].autore.username

    return [diz_freq, autore]


def carica_blacklist(username):
    """
    Funzione che dato un username fa una query al database e restituisce
    la blacklist associata a quell'utente come lista di parole.

    :param username: nome utente presente nel db.
    :return: blacklist dell'utente come lista di parole.
    """
    blacklist = Utenti.objects.get(username=username).blacklist

    if blacklist != '':
        blacklist = [word for word in blacklist.split(',')]
    else:
        blacklist = []

    return blacklist[:-1]


def carica_l_min(username):
    """
    Funzione che dato un username fa una query al database e restituisce
    la blacklist associata a quell'utente come lista di parole.

    :param username: nome utente presente nel db.
    :return: lunghezza minima delle parole da visualizzare per l'utente.
    """
    l_min = Utenti.objects.get(username=username).l_min

    return l_min


def carica_gulpease(titolo):
    """
    Funzione che dato un titolo di un testo fa una query al database e
    restituisce il suo indice di gulpease.

    :param titolo: titolo di un testo presente nel database.
    :return: il suo indice di gulpease.
    """
    return Testi.objects.get(titolo=titolo).indice_legg


def carica_legacy():
    """
    Funzione che fa una query al database e restituisce un dizionario
    che ad ogni parola ha associata la frequenza totale di quella parola
    in tutti i testi presenti nel database.

    :return: dizionario parola -> frequenza totale nel database.
    """
    contiene = list(Contiene.objects.all().select_related('parola'))
    legacy = dict()

    for el in contiene:
        legacy[el.parola.parola] = legacy.get(el.parola.parola, 0) + el.freq

    return legacy
