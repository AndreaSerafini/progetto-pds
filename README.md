# TEAM6

(Si consiglia di consultare il pdf "Analisi.pdf" (cartella doc) per una formattazione migliore, oltre alla tabella delle user stories e alle criticità del progetto.)


LINK A TRELLO CON USER STORIES (Aggiornato al 11/05/2020): https://trello.com/b/j1vlo0Ua/backlog

LINK A PROTOTIPO FUNZIONANTE: http://team6analizzavocabolario.pythonanywhere.com/

Team 6.

Andrea Serafini – Mat. 117165

Alessia Golfieri – Mat. 117949

Lorenzo Lamazzi – Mat. 101353

Mirko Cangiano – Mat. 124465

Gabriele Rinaldini - Mat. 117205

Fase progettuale:

Durante la fase progettuale l’intero team è stato coinvolto, in modo tale da avere chiari tutti i punti principali del progetto.

Prendendo a esempio la progettazione agile è stato dedicato sufficiente tempo per il brainstorming delle user stories,

infine tutte le US sono state valutate con un planning poker.

Ognuno ha proposto vari schemi UML e sono stati discussi i giorni seguenti.

Il modello di sviluppo scelto è il modello SCRUM.

Fase di sviluppo:

Lo sviluppo software è stato suddiviso per capacità e preferenze in questo modo, mantenendo comunque la collaborazione tra membri:

· Mirko Cangiano : Team di sviluppo, Product Owner

· Alessia Golfieri :  Team di sviluppo

· Gabriele Rinaldini :  Team di sviluppo, Scrum Master

· Andrea Serafini :Team di sviluppo

· Lorenzo Lamazzi : Team di sviluppo

Sprint 1 -

· Mirko Cangiano : Implementazione algoritmo analisi del testo, unit testing dell'algoritmo, Product Owner

· Alessia Golfieri :  Frontend visualizzazione testo elaborato

· Gabriele Rinaldini :  Frontend home minimale,integration testing , Scrum master

· Andrea Serafini :Implementazione algoritmo analisi della complessità, unit testing dell'algoritmo

· Lorenzo Lamazzi : Frontend Inserimento testo, integration testing

Sprint 2 (prima settimana)-

· Mirko Cangiano : Progettazione e implementazione Database, blacklist backend, filtro N lettere backend, ricerca testi backend, Product Owner

· Alessia Golfieri :  Frontend e implementazione login, NavBar per la navigazione nel sito, , implementazione link a google translate

· Gabriele Rinaldini :  Frontend Ricerca testi , integration testing ,  Scrum master

· Andrea Serafini : Progettazione e implementazione Database, blacklist backend, filtro N lettere backend, ricerca testi backend, unit testing moduli beckend

· Lorenzo Lamazzi : Frontend confronto testi, Frontend Blacklist, integration testing

Sprint 2 (seconda settimana)-

· Mirko Cangiano : Backend legacy, Aggiornamento ricerca testi,  Product Owner

· Alessia Golfieri :  Implementazione di attivazione account tramite mail , frontend ordinamento ,reset password

· Gabriele Rinaldini :  Aggiornamento ricerca testi, Aggiornamento home, frontend legacy,  Scrum master

· Andrea Serafini : Backend e frontend legacy, unit testing moduli beckend

· Lorenzo Lamazzi : Frontend grafici, frontend ordinamento , unit testing moduli frontend

Viene utilizzata la tecnica del pair programming, ulteriori dettagli saranno presenti nei log individuali.


ANALISI DEL PROBLEMA - DESCRIZIONE DELLA SOLUZIONE PROPOSTA

Si propone un’applicazione web che fornisca uno strumento di analisi del testo per una comunità di utenti registrati. 
Ogni utente, incollando un testo in una casella dell’applicazione,
deve poterne visualizzare i relativi dati delle parole significative e deve poter confrontare i risultati con quelli di testi già analizzati in passato.
Le parole dei testi verranno salvate nel database dell’applicazione per poterle poi confrontare e analizzare in un secondo momento.
L’utente deve, inoltre, poter ricercare testi salvati precedentemente, anche da altri utenti.

Di ogni testo potrà essere visibile l’elenco delle parole significative contenute, con la relativa frequenza, complessità e la traduzione in altre lingue.
Opzionalmente potranno essere inclusi anche i sinonimi di ogni parola.

Per il calcolo della complessità delle parole è stata richiesta l’implementazione di una formula.
Abbiamo intenzione di utilizzare una formula che valuti ogni parola in base alla propria rarità.

Dalla descrizione del cliente abbiamo estratto i seguenti requisiti:

· Web - App in cui copiare testi per calcolarne la frequenza di ogni parola

· Creazione di un database che contenga i testi (de-strutturati in parole)

· Analisi di complessità delle parole, da cui deriva il confronto di complessità dei testi

· Trovare traduzioni ed eventualmente sinonimi di ogni parola
