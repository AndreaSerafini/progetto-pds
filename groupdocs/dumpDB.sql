--
-- PostgreSQL database dump
--

-- Dumped from database version 11.6
-- Dumped by pg_dump version 12.2

-- Started on 2020-05-24 10:52:05

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 2 (class 3079 OID 16612)
-- Name: pg_trgm; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS pg_trgm WITH SCHEMA public;


--
-- TOC entry 4014 (class 0 OID 0)
-- Dependencies: 2
-- Name: EXTENSION pg_trgm; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION pg_trgm IS 'text similarity measurement and index searching based on trigrams';


SET default_tablespace = '';

--
-- TOC entry 204 (class 1259 OID 16432)
-- Name: auth_group; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_group (
    id integer NOT NULL,
    name character varying(150) NOT NULL
);


ALTER TABLE public.auth_group OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 16430)
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_id_seq OWNER TO postgres;

--
-- TOC entry 4015 (class 0 OID 0)
-- Dependencies: 203
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_group_id_seq OWNED BY public.auth_group.id;


--
-- TOC entry 206 (class 1259 OID 16442)
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_group_permissions OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 16440)
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_group_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_permissions_id_seq OWNER TO postgres;

--
-- TOC entry 4016 (class 0 OID 0)
-- Dependencies: 205
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_group_permissions_id_seq OWNED BY public.auth_group_permissions.id;


--
-- TOC entry 202 (class 1259 OID 16424)
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE public.auth_permission OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 16422)
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_permission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_permission_id_seq OWNER TO postgres;

--
-- TOC entry 4017 (class 0 OID 0)
-- Dependencies: 201
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_permission_id_seq OWNED BY public.auth_permission.id;


--
-- TOC entry 216 (class 1259 OID 16517)
-- Name: contiene; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.contiene (
    id integer NOT NULL,
    freq integer NOT NULL,
    parola_id character varying(20) NOT NULL,
    titolo_id character varying(100) NOT NULL,
    CONSTRAINT contiene_freq_check CHECK ((freq >= 0))
);


ALTER TABLE public.contiene OWNER TO postgres;

--
-- TOC entry 215 (class 1259 OID 16515)
-- Name: contiene_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.contiene_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.contiene_id_seq OWNER TO postgres;

--
-- TOC entry 4018 (class 0 OID 0)
-- Dependencies: 215
-- Name: contiene_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.contiene_id_seq OWNED BY public.contiene.id;


--
-- TOC entry 218 (class 1259 OID 16579)
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE public.django_admin_log OWNER TO postgres;

--
-- TOC entry 217 (class 1259 OID 16577)
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.django_admin_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_admin_log_id_seq OWNER TO postgres;

--
-- TOC entry 4019 (class 0 OID 0)
-- Dependencies: 217
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.django_admin_log_id_seq OWNED BY public.django_admin_log.id;


--
-- TOC entry 200 (class 1259 OID 16414)
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE public.django_content_type OWNER TO postgres;

--
-- TOC entry 199 (class 1259 OID 16412)
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.django_content_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_content_type_id_seq OWNER TO postgres;

--
-- TOC entry 4020 (class 0 OID 0)
-- Dependencies: 199
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.django_content_type_id_seq OWNED BY public.django_content_type.id;


--
-- TOC entry 198 (class 1259 OID 16403)
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.django_migrations OWNER TO postgres;

--
-- TOC entry 197 (class 1259 OID 16401)
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.django_migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_migrations_id_seq OWNER TO postgres;

--
-- TOC entry 4021 (class 0 OID 0)
-- Dependencies: 197
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.django_migrations_id_seq OWNED BY public.django_migrations.id;


--
-- TOC entry 219 (class 1259 OID 16601)
-- Name: django_session; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE public.django_session OWNER TO postgres;

--
-- TOC entry 207 (class 1259 OID 16474)
-- Name: parole; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.parole (
    parola character varying(20) NOT NULL
);


ALTER TABLE public.parole OWNER TO postgres;

--
-- TOC entry 214 (class 1259 OID 16509)
-- Name: testi; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.testi (
    titolo character varying(100) NOT NULL,
    indice_legg integer NOT NULL,
    data date NOT NULL,
    ora time without time zone NOT NULL,
    autore_id integer NOT NULL,
    CONSTRAINT testi_indice_legg_check CHECK ((indice_legg >= 0))
);


ALTER TABLE public.testi OWNER TO postgres;

--
-- TOC entry 209 (class 1259 OID 16481)
-- Name: utenti; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.utenti (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(150) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(150) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL,
    bio text NOT NULL,
    blacklist text NOT NULL,
    l_min integer NOT NULL,
    CONSTRAINT utenti_l_min_check CHECK ((l_min >= 0))
);


ALTER TABLE public.utenti OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 16495)
-- Name: utenti_groups; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.utenti_groups (
    id integer NOT NULL,
    utenti_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.utenti_groups OWNER TO postgres;

--
-- TOC entry 210 (class 1259 OID 16493)
-- Name: utenti_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.utenti_groups_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.utenti_groups_id_seq OWNER TO postgres;

--
-- TOC entry 4022 (class 0 OID 0)
-- Dependencies: 210
-- Name: utenti_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.utenti_groups_id_seq OWNED BY public.utenti_groups.id;


--
-- TOC entry 208 (class 1259 OID 16479)
-- Name: utenti_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.utenti_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.utenti_id_seq OWNER TO postgres;

--
-- TOC entry 4023 (class 0 OID 0)
-- Dependencies: 208
-- Name: utenti_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.utenti_id_seq OWNED BY public.utenti.id;


--
-- TOC entry 213 (class 1259 OID 16503)
-- Name: utenti_user_permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.utenti_user_permissions (
    id integer NOT NULL,
    utenti_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.utenti_user_permissions OWNER TO postgres;

--
-- TOC entry 212 (class 1259 OID 16501)
-- Name: utenti_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.utenti_user_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.utenti_user_permissions_id_seq OWNER TO postgres;

--
-- TOC entry 4024 (class 0 OID 0)
-- Dependencies: 212
-- Name: utenti_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.utenti_user_permissions_id_seq OWNED BY public.utenti_user_permissions.id;


--
-- TOC entry 3802 (class 2604 OID 16435)
-- Name: auth_group id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group ALTER COLUMN id SET DEFAULT nextval('public.auth_group_id_seq'::regclass);


--
-- TOC entry 3803 (class 2604 OID 16445)
-- Name: auth_group_permissions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_group_permissions_id_seq'::regclass);


--
-- TOC entry 3801 (class 2604 OID 16427)
-- Name: auth_permission id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_permission ALTER COLUMN id SET DEFAULT nextval('public.auth_permission_id_seq'::regclass);


--
-- TOC entry 3809 (class 2604 OID 16520)
-- Name: contiene id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.contiene ALTER COLUMN id SET DEFAULT nextval('public.contiene_id_seq'::regclass);


--
-- TOC entry 3811 (class 2604 OID 16582)
-- Name: django_admin_log id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_admin_log ALTER COLUMN id SET DEFAULT nextval('public.django_admin_log_id_seq'::regclass);


--
-- TOC entry 3800 (class 2604 OID 16417)
-- Name: django_content_type id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_content_type ALTER COLUMN id SET DEFAULT nextval('public.django_content_type_id_seq'::regclass);


--
-- TOC entry 3799 (class 2604 OID 16406)
-- Name: django_migrations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_migrations ALTER COLUMN id SET DEFAULT nextval('public.django_migrations_id_seq'::regclass);


--
-- TOC entry 3804 (class 2604 OID 16484)
-- Name: utenti id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.utenti ALTER COLUMN id SET DEFAULT nextval('public.utenti_id_seq'::regclass);


--
-- TOC entry 3806 (class 2604 OID 16498)
-- Name: utenti_groups id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.utenti_groups ALTER COLUMN id SET DEFAULT nextval('public.utenti_groups_id_seq'::regclass);


--
-- TOC entry 3807 (class 2604 OID 16506)
-- Name: utenti_user_permissions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.utenti_user_permissions ALTER COLUMN id SET DEFAULT nextval('public.utenti_user_permissions_id_seq'::regclass);


--
-- TOC entry 3826 (class 2606 OID 16472)
-- Name: auth_group auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- TOC entry 3831 (class 2606 OID 16458)
-- Name: auth_group_permissions auth_group_permissions_group_id_permission_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- TOC entry 3834 (class 2606 OID 16447)
-- Name: auth_group_permissions auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- TOC entry 3828 (class 2606 OID 16437)
-- Name: auth_group auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- TOC entry 3821 (class 2606 OID 16449)
-- Name: auth_permission auth_permission_content_type_id_codename_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- TOC entry 3823 (class 2606 OID 16429)
-- Name: auth_permission auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- TOC entry 3862 (class 2606 OID 16523)
-- Name: contiene contiene_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.contiene
    ADD CONSTRAINT contiene_pkey PRIMARY KEY (id);


--
-- TOC entry 3866 (class 2606 OID 16562)
-- Name: contiene contiene_titolo_id_parola_id_4070fe1f_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.contiene
    ADD CONSTRAINT contiene_titolo_id_parola_id_4070fe1f_uniq UNIQUE (titolo_id, parola_id);


--
-- TOC entry 3869 (class 2606 OID 16588)
-- Name: django_admin_log django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- TOC entry 3816 (class 2606 OID 16421)
-- Name: django_content_type django_content_type_app_label_model_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- TOC entry 3818 (class 2606 OID 16419)
-- Name: django_content_type django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- TOC entry 3814 (class 2606 OID 16411)
-- Name: django_migrations django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- TOC entry 3873 (class 2606 OID 16608)
-- Name: django_session django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- TOC entry 3837 (class 2606 OID 16478)
-- Name: parole parole_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.parole
    ADD CONSTRAINT parole_pkey PRIMARY KEY (parola);


--
-- TOC entry 3857 (class 2606 OID 16514)
-- Name: testi testi_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.testi
    ADD CONSTRAINT testi_pkey PRIMARY KEY (titolo);


--
-- TOC entry 3845 (class 2606 OID 16500)
-- Name: utenti_groups utenti_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.utenti_groups
    ADD CONSTRAINT utenti_groups_pkey PRIMARY KEY (id);


--
-- TOC entry 3848 (class 2606 OID 16527)
-- Name: utenti_groups utenti_groups_utenti_id_group_id_37ff1244_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.utenti_groups
    ADD CONSTRAINT utenti_groups_utenti_id_group_id_37ff1244_uniq UNIQUE (utenti_id, group_id);


--
-- TOC entry 3839 (class 2606 OID 16490)
-- Name: utenti utenti_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.utenti
    ADD CONSTRAINT utenti_pkey PRIMARY KEY (id);


--
-- TOC entry 3851 (class 2606 OID 16508)
-- Name: utenti_user_permissions utenti_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.utenti_user_permissions
    ADD CONSTRAINT utenti_user_permissions_pkey PRIMARY KEY (id);


--
-- TOC entry 3854 (class 2606 OID 16541)
-- Name: utenti_user_permissions utenti_user_permissions_utenti_id_permission_id_7478f740_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.utenti_user_permissions
    ADD CONSTRAINT utenti_user_permissions_utenti_id_permission_id_7478f740_uniq UNIQUE (utenti_id, permission_id);


--
-- TOC entry 3842 (class 2606 OID 16492)
-- Name: utenti utenti_username_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.utenti
    ADD CONSTRAINT utenti_username_key UNIQUE (username);


--
-- TOC entry 3824 (class 1259 OID 16473)
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_group_name_a6ea08ec_like ON public.auth_group USING btree (name varchar_pattern_ops);


--
-- TOC entry 3829 (class 1259 OID 16469)
-- Name: auth_group_permissions_group_id_b120cbf9; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON public.auth_group_permissions USING btree (group_id);


--
-- TOC entry 3832 (class 1259 OID 16470)
-- Name: auth_group_permissions_permission_id_84c5c92e; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON public.auth_group_permissions USING btree (permission_id);


--
-- TOC entry 3819 (class 1259 OID 16455)
-- Name: auth_permission_content_type_id_2f476e4b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_permission_content_type_id_2f476e4b ON public.auth_permission USING btree (content_type_id);


--
-- TOC entry 3859 (class 1259 OID 16573)
-- Name: contiene_parola_id_f01f5c50; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX contiene_parola_id_f01f5c50 ON public.contiene USING btree (parola_id);


--
-- TOC entry 3860 (class 1259 OID 16574)
-- Name: contiene_parola_id_f01f5c50_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX contiene_parola_id_f01f5c50_like ON public.contiene USING btree (parola_id varchar_pattern_ops);


--
-- TOC entry 3863 (class 1259 OID 16575)
-- Name: contiene_titolo_id_291fd810; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX contiene_titolo_id_291fd810 ON public.contiene USING btree (titolo_id);


--
-- TOC entry 3864 (class 1259 OID 16576)
-- Name: contiene_titolo_id_291fd810_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX contiene_titolo_id_291fd810_like ON public.contiene USING btree (titolo_id varchar_pattern_ops);


--
-- TOC entry 3867 (class 1259 OID 16599)
-- Name: django_admin_log_content_type_id_c4bce8eb; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON public.django_admin_log USING btree (content_type_id);


--
-- TOC entry 3870 (class 1259 OID 16600)
-- Name: django_admin_log_user_id_c564eba6; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_admin_log_user_id_c564eba6 ON public.django_admin_log USING btree (user_id);


--
-- TOC entry 3871 (class 1259 OID 16610)
-- Name: django_session_expire_date_a5c62663; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_session_expire_date_a5c62663 ON public.django_session USING btree (expire_date);


--
-- TOC entry 3874 (class 1259 OID 16609)
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_session_session_key_c0390e0f_like ON public.django_session USING btree (session_key varchar_pattern_ops);


--
-- TOC entry 3835 (class 1259 OID 16524)
-- Name: parole_parola_550bce91_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX parole_parola_550bce91_like ON public.parole USING btree (parola varchar_pattern_ops);


--
-- TOC entry 3855 (class 1259 OID 16560)
-- Name: testi_autore_id_71cc3b44; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX testi_autore_id_71cc3b44 ON public.testi USING btree (autore_id);


--
-- TOC entry 3858 (class 1259 OID 16559)
-- Name: testi_titolo_7d627798_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX testi_titolo_7d627798_like ON public.testi USING btree (titolo varchar_pattern_ops);


--
-- TOC entry 3843 (class 1259 OID 16539)
-- Name: utenti_groups_group_id_744fe092; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX utenti_groups_group_id_744fe092 ON public.utenti_groups USING btree (group_id);


--
-- TOC entry 3846 (class 1259 OID 16538)
-- Name: utenti_groups_utenti_id_1a55c3cf; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX utenti_groups_utenti_id_1a55c3cf ON public.utenti_groups USING btree (utenti_id);


--
-- TOC entry 3849 (class 1259 OID 16553)
-- Name: utenti_user_permissions_permission_id_27e40a8d; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX utenti_user_permissions_permission_id_27e40a8d ON public.utenti_user_permissions USING btree (permission_id);


--
-- TOC entry 3852 (class 1259 OID 16552)
-- Name: utenti_user_permissions_utenti_id_eb1216c3; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX utenti_user_permissions_utenti_id_eb1216c3 ON public.utenti_user_permissions USING btree (utenti_id);


--
-- TOC entry 3840 (class 1259 OID 16525)
-- Name: utenti_username_c31bba52_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX utenti_username_c31bba52_like ON public.utenti USING btree (username varchar_pattern_ops);


--
-- TOC entry 3877 (class 2606 OID 16464)
-- Name: auth_group_permissions auth_group_permissio_permission_id_84c5c92e_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3876 (class 2606 OID 16459)
-- Name: auth_group_permissions auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3875 (class 2606 OID 16450)
-- Name: auth_permission auth_permission_content_type_id_2f476e4b_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3883 (class 2606 OID 16563)
-- Name: contiene contiene_parola_id_f01f5c50_fk_parole_parola; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.contiene
    ADD CONSTRAINT contiene_parola_id_f01f5c50_fk_parole_parola FOREIGN KEY (parola_id) REFERENCES public.parole(parola) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3884 (class 2606 OID 16568)
-- Name: contiene contiene_titolo_id_291fd810_fk_testi_titolo; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.contiene
    ADD CONSTRAINT contiene_titolo_id_291fd810_fk_testi_titolo FOREIGN KEY (titolo_id) REFERENCES public.testi(titolo) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3885 (class 2606 OID 16589)
-- Name: django_admin_log django_admin_log_content_type_id_c4bce8eb_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3886 (class 2606 OID 16594)
-- Name: django_admin_log django_admin_log_user_id_c564eba6_fk_utenti_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_utenti_id FOREIGN KEY (user_id) REFERENCES public.utenti(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3882 (class 2606 OID 16554)
-- Name: testi testi_autore_id_71cc3b44_fk_utenti_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.testi
    ADD CONSTRAINT testi_autore_id_71cc3b44_fk_utenti_id FOREIGN KEY (autore_id) REFERENCES public.utenti(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3879 (class 2606 OID 16533)
-- Name: utenti_groups utenti_groups_group_id_744fe092_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.utenti_groups
    ADD CONSTRAINT utenti_groups_group_id_744fe092_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3878 (class 2606 OID 16528)
-- Name: utenti_groups utenti_groups_utenti_id_1a55c3cf_fk_utenti_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.utenti_groups
    ADD CONSTRAINT utenti_groups_utenti_id_1a55c3cf_fk_utenti_id FOREIGN KEY (utenti_id) REFERENCES public.utenti(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3881 (class 2606 OID 16547)
-- Name: utenti_user_permissions utenti_user_permissi_permission_id_27e40a8d_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.utenti_user_permissions
    ADD CONSTRAINT utenti_user_permissi_permission_id_27e40a8d_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3880 (class 2606 OID 16542)
-- Name: utenti_user_permissions utenti_user_permissions_utenti_id_eb1216c3_fk_utenti_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.utenti_user_permissions
    ADD CONSTRAINT utenti_user_permissions_utenti_id_eb1216c3_fk_utenti_id FOREIGN KEY (utenti_id) REFERENCES public.utenti(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 4013 (class 0 OID 0)
-- Dependencies: 4
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM rdsadmin;
REVOKE ALL ON SCHEMA public FROM PUBLIC;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2020-05-24 10:52:17

--
-- PostgreSQL database dump complete
--

