# To use thi unit test module:
#  - Install selenium (pip3 install selenium)
#  - Install Google Chrome
#  - Download Google Chrome drivers (https://sites.google.com/a/chromium.org/chromedriver/downloads)
#  - Put Google Chrome's drivers into $PATH

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By

driver = webdriver.Chrome()
driver.maximize_window()


"""
LOGIN
"""
driver.get("http://team6analizzavocabolario.pythonanywhere.com/login")
name = driver.find_element_by_name("username")
password = driver.find_element_by_name("password")
#Inserimento password errata
name.send_keys("lore")
password.send_keys("ciaobelli")
accedi = driver.find_element(By.XPATH, '//button[text()="Accedi"]')
accedi.click()

#Inserimento password corretta
name = driver.find_element_by_name("username")
password = driver.find_element_by_name("password")
accedi = driver.find_element(By.XPATH, '//button[text()="Accedi"]')
name.clear()
password.clear()
#Inserimento password corretta
name.send_keys("lore")
password.send_keys("jjvCED2QbGDtmqS")
accedi.click()


"""
INSERIMENTO TESTO
"""
titolo = "I promessi sposi"
assert 'Inserisci testo' in driver.title

text_box_titolo = driver.find_element_by_name("titolo")
text_box_testo = driver.find_element_by_name("testo")
btn_elabora = driver.find_element_by_name("elabora")

text_box_titolo.send_keys(titolo)
text_box_testo.send_keys("Quel ramo del lago di como che punta a mezzogiorno mezzogiorno mezzogiorno")
btn_elabora.click()

"""
ORDINAMENTO
"""
order = driver.find_element_by_name("ordine")
order_button = driver.find_element_by_name("ordina")
#Ordinamento alfabetico
#order.send_keys("Ordine alfabetico")
#order_button.click()
#Ordinamento frequenza
order.send_keys("Frequenza decrescente")
order_button.click()


"""
GRAFICO
"""
grafico = driver.find_element_by_name("grafico")
grafico.click()

#driver.get("http://team6analizzavocabolario.pythonanywhere.com/blacklist")
#lun_min = driver.find_element_by_name("lunghezzaminima")
#lun_min.send_keys("3")
#aggiorna = driver.find_element_by_name("aggiorna")
#aggiorna.click()

# se non sei loggato ti fa loggare

"""
Ricerca Testi
"""

driver.get("http://team6analizzavocabolario.pythonanywhere.com/ricerca")
barra_ricerca = driver.find_element_by_name("barraRicerca")
text_ricerca = "paper"
cerca_button = driver.find_element(By.XPATH, '//button[text()="Cerca"]')

#Ricerca di un testo
barra_ricerca.send_keys(text_ricerca)
cerca_button.click()

#Aggiungo al confronto il testo cercato
aggiungi_confronto = driver.find_element_by_name("aggiungi")
aggiungi_confronto.click()

#Ricerca di un secondo testo
barra_ricerca = driver.find_element_by_name("barraRicerca")
text_ricerca2 = "storia"
cerca_button = driver.find_element(By.XPATH, '//button[text()="Cerca"]')


barra_ricerca.send_keys(text_ricerca2)
cerca_button.click()

#Aggiungo al confronto un secondo testo
aggiungi_confronto = driver.find_element_by_name("aggiungi")
aggiungi_confronto.click()

#Reset dei testi aggiunti al confronto
#reset_button = driver.find_element_by_name("reset")
#reset_button.click()

#Confronto di due testi
confronto = driver.find_element_by_name("confronto")
confronto.click()

#assert 'Visualizza ' in driver.title