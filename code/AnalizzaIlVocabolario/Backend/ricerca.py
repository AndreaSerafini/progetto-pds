from django.contrib.postgres.search import TrigramSimilarity

from AnalizzaIlVocabolario.models import Testi



def cerca(titolo):
    """
    Funzione che prende in input un titolo ed effettua la ricerca su DataBase dei titoli dei testi basata su trigram
    similarity. Restituisce la lista dei titoli compatibili con la ricerca (max 10 titoli).

    :param titolo: stringa contenente il titolo da cercare
    :return: lista dei risultati della ricerca
    """
    risultato = list(Testi.objects.annotate(similarity=TrigramSimilarity('titolo', titolo), ) \
                     .filter(similarity__gt=0.2).order_by('-similarity'))[:10]

    return [t.titolo for t in risultato]
