import os
import django
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'AnalizzaIlVocabolario.settings')
django.setup()
from Backend.dbload import carica_blacklist, carica_l_min, carica_diz_freq, carica_gulpease, carica_legacy
from AnalizzaIlVocabolario.models import Contiene, Utenti, Testi, Parole

# Precedentemente abbiamo salvato un testo nel sito con questo contenuto
# Titolo: Test
# Testo: Questo è il testo che utilizzeremo per il test, il testo è incompleto

titolo = "Test"
testo = "Ciao mamma, guarda come mi diverto"
autore = "andreas"
gulpease = 69
diz_freq = {
    'questo': 1,
    'è': 2,
    'il': 3,
    'testo': 2,
    'che': 1,
    'utilizzeremo': 1,
    'per': 1,
    'test': 1,
    'incompleto': 1
}


def test_carica_diz_freq():
    ris = carica_diz_freq(titolo)
    assert ris[0] == diz_freq
    assert ris[1] == autore


def test_gulpease():
    ris = carica_gulpease(titolo)
    assert ris == 69


# Account test:
# User: test
# Psw: test_1234
# l_min: 3
# blacklist: ["ciao", "mamma", "fratello"]
def test_l_min():
    user = "test"
    ris = carica_l_min(user)
    assert ris == 3


def test_blacklist():
    user = "test"
    ris = carica_blacklist(user)
    assert ris == ["ciao", "mamma", "fratello"]


def test_carica_legacy():
    parola = "mamma"
    somma = 0

    for testo in Testi.objects.all():
        d = carica_diz_freq(testo.titolo)[0]
        somma += d.get(parola, 0)

    assert somma == carica_legacy().get(parola, 0)
