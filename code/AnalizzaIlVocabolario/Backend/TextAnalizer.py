import nltk
from nltk.corpus import stopwords
from nltk.tokenize import sent_tokenize
import datetime
import string
import re

nltk.download('punkt')


def remove_all_not_letters(text):
    """
    Funzione che prende in input una stringa qualisasi
    e rimuove tutti i caratteri non ascii-letters + vocali accentate.
    Restituisce una stringa.

    :param text: una stringa qualisasi.
    :return: una stringa con solo caratteri ascii-letters e lettere accentate.
    """
    for letter in text:
        if letter not in string.ascii_letters + "àèéìòù":
            text = text.replace(letter, ' ')
    return text


def remove_puntuation(text):
    """
    Funzione che prende in input una stringa qualsiasi e ne rimuove la punteggiatura,
    restituendo una stringa.

    :param text: una stringa qualsiasi.
    :return: una stringa senza punteggiatura.
    """
    text_without_punctuation = text.translate(str.maketrans(dict([(key, ' ') for key in string.punctuation])))

    return text_without_punctuation


def remove_numbers(text):
    """
    Funzione che prende in input una stringa qualsiasi e ne rimuove le occorrenze
    di caratteri che rappresentano i numeri decimali, restituendo una stringa.

    :param text: una stringa qualsiasi.
    :return: una stringa senza numeri decimali.
    """
    text_without_numbers = text.translate(str.maketrans(dict([(key, ' ') for key in '1234567890'])))

    return text_without_numbers


def count_words_frequency(words_list):
    """
    Funzione che prende in input una lista di parole e restituisce un dizionario
    le cui chiavi sono le parole distinte presenti nella lista e i valori le frequenze
    di quelle parole nella lista.

    :param words_list: una lista di parola.
    :return: dizionario parola->frequenza nella lista iniziale.
    """
    words_with_frequency = dict()

    for word in words_list:
        words_with_frequency[word] = words_with_frequency.get(word, 0) + 1

    return words_with_frequency


def count_letters_in_word_list(word_list):
    """
    Funzione che prende in input una lista di parola e restituisce il numero di lettere totali

    :param word_list: lista di parole.
    :return: numero di lettere totali.
    """
    num_letters = sum([len(word) for word in word_list])

    return num_letters


def count_phrases_in_text(text):
    """
    Funzione che prende in input una stringa qualsiasi e restituisce il numero
    di frasi totali

    :param text: una stringa qualsiasi.
    :return: numero di frasi totali.
    """
    formatted_text = text.translate(str.maketrans({'.': '. '}))
    num_phrases = len(sent_tokenize(formatted_text))

    return num_phrases


def compute_gulpease_index(num_phrases, num_letters, num_words):
    """
    Funzione che prende in input il numero di frasi, lettere e parole di un testo
    e restituisce l'indice di gulpease calcolato.

    :param num_phrases: numero di frasi di un testo.
    :param num_letters: numero di lettere in un testo.
    :param num_words: numero di parole in un testo.
    :return: indice di gulpease.
    """
    if num_words == 0:
        return 0

    gulpease_index = 89 + ((300 * num_phrases - 10 * num_letters) / num_words)

    return max(min(int(gulpease_index), 100), 0)


def tokenize_text(text):
    """
    Funzione che prende in input una stringa che rappresenta un testo
    e la tokenizza creando una lista delle sue singole parole.
    Restituisce la lista appena creata.
    :param text: stringa che rappresenta un testo.
    :return: lista di parole.
    """
    tokenized_text = []

    for word in nltk.word_tokenize(text, language="italian"):
        word = word.lower()
        tokenized_text.append(word)

    return tokenized_text


def apply_minchar_filter(words_with_frequency, minchar):
    """
    Funzione che prende in input una dizionario con parole e relative frequenze
    e gli applica un filtro rimuovendo tutte le parole di lunghezza minore ad un
    valore passato come parametro.
    Restituisce un nuovo dizionario filtrato.

    :param words_with_frequency: dizionario con chiave una parola e valore la sua frequenza.
    :param minchar: lunghezza minima delle parole da mostrare.
    :return: dizionario filtrato.
    """
    words_to_show = dict()

    for word in words_with_frequency:
        if len(word) > minchar:
            words_to_show[word] = words_with_frequency[word]
    return words_to_show


def apply_blacklist_filter(words_with_frequency, blacklist):
    """
    Funzione che prende in input una dizionario con parole e relative frequenze
    e gli applica un filtro rimuovendo tutte le parole presenti in una blacklist
    passata come parametro. Restituisce un nuovo dizionario filtrato.

    :param words_with_frequency:
    :param blacklist:
    :return:
    """
    words_to_show = dict()

    for word in words_with_frequency:
        if word not in blacklist:
            words_to_show[word] = words_with_frequency[word]

    return words_to_show


class Text():
    def __init__(self, title, text):
        self.title = title
        self.gulpease_index = self.complexity(text)
        self.words_with_frequency = self.analyze(text)

    def analyze(self, text):
        """
        Funzione di classe che prende in input una stringa qualsiasi, ne rimuove
        punteggiatura, occorrenze di valori numerici decimali e restituisce una
        dizionario delle parole rimaste e le loro occorrenze nella stringa iniziale.

        :param text: una stringa qualsiasi.
        :return: dizionario parola->la sua frequenza nella stringa iniziale.
        """
        text_without_punctuation = remove_puntuation(text)
        text_without_numbers = remove_numbers(text_without_punctuation)
        text_with_only_letters = remove_all_not_letters(text_without_numbers)
        tokenized_text = tokenize_text(text_with_only_letters)

        words_with_frequency = count_words_frequency(tokenized_text)

        return words_with_frequency

    def complexity(self, text):
        """
        Funzione di classe che prende in input una stringa qualsiasi e
        restituisce l'indice di gulpease calcolato.

        :param text: stringa qualsiasi.
        :return: indice di gulpease.
        """
        num_phrases = count_phrases_in_text(text)

        text_without_punctuation = remove_puntuation(text)
        text_without_numbers = remove_numbers(text_without_punctuation)

        word_list = nltk.word_tokenize(text_without_numbers)

        num_letters = count_letters_in_word_list(word_list)
        num_words = len(word_list)

        gulpease_index = compute_gulpease_index(num_phrases, num_letters, num_words)

        return gulpease_index

    def __str__(self):
        return str(self.words_with_frequency)
