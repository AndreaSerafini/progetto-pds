from AnalizzaIlVocabolario.models import Testi, Contiene, Parole
from Backend.dbload import carica_diz_freq


def unisci_testi(titoli):
    """
    Funzione che prende in input una lista di titoli da confrontare e restituisce un dizionario
    di liste di frequenze per ogni parola contenuta nei testi, se un testo non possiede quella parola
    la frequenza è 0.

    :param titoli: lista di stringhe.
    :return: dizionario chiave-parola valore-lista.
    """
    lista_diz_freq = dict()

    for titolo in titoli:
        lista_diz_freq[titolo] = carica_diz_freq(titolo)[0]

    parole = set()
    for diz_freq in lista_diz_freq:
        for parola in lista_diz_freq[diz_freq]:
            parole.add(parola)

    tabella_testi = dict()
    for parola in parole:
        tabella_testi[parola] = []

        for titolo in titoli:
            tabella_testi[parola].append((titolo, lista_diz_freq[titolo].get(parola, 0)))

    return tabella_testi


def indici_gulpease(titoli):
    """
    Funzione che prende in input una lista di titoli e restituisce una lista di tuple
    contenente le informazioni del testo.

    :param titoli: lista di stringhe.
    :return: lista di tuple (titolo,indice gulpease,autore).
    """
    gulpease_list = list()

    for titolo in titoli:
        testo = Testi.objects.get(titolo=titolo)
        gulpease_list.append((titolo, testo.indice_legg, testo.autore.username))

    return gulpease_list
