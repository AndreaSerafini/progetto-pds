from AnalizzaIlVocabolario.models import Testi, Parole, Contiene, Utenti


def salva_testo(titolo, gulpease, frequenza, autore):
    """
    Funzione che salva tutti i dati relativi ad un testo nel database.

    :param titolo: stringa titolo.
    :param gulpease: indice di gulpease.
    :param frequenza: dizionario parola -> frequenza nel testo.
    :param autore: nome utente di chi ha caricato il testo.
    :return: nessun valore di ritorno.
    """
    user = Utenti.objects.filter(username=autore)[0]

    tupla_testo = Testi(titolo=titolo, autore=user, indice_legg=gulpease)
    tupla_testo.save()

    parole = list()
    relazioni = list()
    all = Parole.objects.all()

    for word in frequenza:
        parola = Parole(parola=word)
        if parola not in all:
            parole.append(parola)

        relazioni.append(Contiene(titolo=tupla_testo, parola=parola, freq=frequenza[word]))

    Parole.objects.bulk_create(parole)
    Contiene.objects.bulk_create(relazioni)


def salva_blacklist(username, blacklist):
    """
    Funzione che sovrascrive il valore della blacklist assoociato ad un utente con
    uno nuovo inserito da input.

    :param username: username dell'utente che aggiorna la blacklist.
    :param blacklist: nuova blacklist, come lista di parole.
    :return: nessun valore di ritorno.
    """
    utente = Utenti.objects.get(username=username)

    blacklist_stringa = ""

    for word in blacklist:
        blacklist_stringa += word + ","

    utente.blacklist = blacklist_stringa
    utente.save()


def salva_l_min(username, l_min):
    """
    Funzione che sovrascrive il valore della lunghezza minima di un testo associato ad un utente con
    una nuova inserita da input.

    :param username: username dell'utente che aggiorna la lunghezza minima.
    :param l_min: nuova lunghezza minima.
    :return: nessun valore di ritorno.
    """
    utente = Utenti.objects.get(username=username)
    utente.l_min = l_min
    utente.save()
