Valentina ha una stanza delle bambole nella sua camera. E’ una vera e propria cameretta di legno, con mobili, sedie, tavolini, il passeggino da bambola e la culla. Valentina ha due bambole: una delle due è sempre brava e l’altra è un po’ monella. Le due bambole, a volte, litigano: la bambola monella fa la prepotente, non segue le regole e non ascolta la bambola brava. Quando Valentina si arrabbia, la bambola monella è ancora più disobbediente.
Arriva il lunedì e Valentina è in collera con tutti.  Al mattino, Valentina non vuole andare all’asilo. Al pomeriggio, non vuole tornare a casa. Valentina è  in collera con le maestre dell’asilo; con la mamma, con i compagni dell’asilo; con la sorellina.
Quando la mamma  va a prendere Valentina all’asilo, la bambina non vuole tornare a casa. La mamma la veste e la porta fuori e Valentina fa i capricci per strada: piange e grida e non vuole camminare.
Quando arrivano a casa, la mamma ha già perso la pazienza.
“Vai in cameretta e prova a calmarti da sola”, dice la mamma e la lascia da sola.
Valentina entra nella stanza delle bambole. Le sue piccole amiche si mettono a litigare e la bambola disobbediente dice alla bimba: “Oggi siamo ancora più arrabbiate!”.

“Sì, oggi voglio buttare tutto in aria!”, dice Valentina.

“Facciamolo!”, dice la bambola. Valentina ha una scatola di giocattoli: li tira tutti fuori  e li lascia  sul pavimento. Tirano fuori i pennarelli dalle confezioni, i pezzi del puzzle di legno.
In cameretta, ora, c’è molta confusione, ma Valentina e la bambola  sono ancora più in collera.
La bambola brava esclama:” Basta! Calmatevi voi due, o verrà la mamma e si arrabbierà davvero!”.
La bambola monella risponde: “Non mi importa. Voglio buttare tutto in aria!”.
“Valentina” , grida la mamma dalla cucina, “che cos’è questo rumore?”.
La voce della mamma è calma, ma curiosa. Valentina sa che tra poco arriverà in camera e allora sarà davvero seccata per tutti i giochi sparsi per terra.
“Bambole”, dice Valentina, “Fate pace, ora”.
“Noi non vogliamo fare pace”, grida la bambola monella.
Troppo tardi. La mamma arriva in camera.
“Mamma”, dice Valentina, “le due bambole hanno litigato. La bambola monella proprio non vuole ascoltare”.
Mamma fa una faccia strana. Dopo,fa un respiro e dice:
“Mettiamo a posto insieme? Uno per uno”.
Mamma mette a posto un giocattolo; dopo, è il turno di Valentina e mette a posto un altro giocattolo, piano piano. Le due bambole fanno pace.