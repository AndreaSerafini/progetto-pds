from django.contrib.auth.forms import UserCreationForm, SetPasswordForm
from django import forms
from .models import Utenti
from django.contrib.auth.forms import PasswordResetForm


class RegistrationForm(UserCreationForm):

    bio = forms.CharField(label='Bio')
    email = forms.EmailField(required=True)

    class Meta:
        model = Utenti
        fields = ('username', 'email', 'password1', 'password2', 'first_name', 'last_name', 'bio')

    def save(self, commit=True):
        user = super(RegistrationForm, self).save(commit=False)
        user.l_min = 0
        user.bio = self.cleaned_data["bio"]
        if commit:
            user.save()
        return user

class BlacklistForm(forms.ModelForm):
    parolablack = forms.CharField(label="Inserisci parola blacklist", max_length=100)
    #class Meta:
     #   model = Utenti


class SearchForm(forms.Form):
    titolo = forms.CharField(label="barraRicerca")
