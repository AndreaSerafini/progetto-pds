AnalizzaIlVocabolario.migrations package
========================================

Submodules
----------

AnalizzaIlVocabolario.migrations.0001\_initial module
-----------------------------------------------------

.. automodule:: AnalizzaIlVocabolario.migrations.0001_initial
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: AnalizzaIlVocabolario.migrations
   :members:
   :undoc-members:
   :show-inheritance:
