from collections import OrderedDict


def ordina(dict_words, ordine):
    """
    Funzione che ordina un dizionario di parole in base al tipo di ordinamento passato
    come parametro.

    :param dict_words: dizionario da ordinare
    :param ordine: tipo di ordine
    :return: dizionario ordinato
    """
    if ordine == "Frequenza crescente":
        dict_words = OrderedDict(sorted(dict_words.items(), key=lambda x: x[1]))
    elif ordine == "Frequenza decrescente":
        dict_words = OrderedDict(sorted(dict_words.items(), key=lambda x: x[1], reverse=True))
    elif ordine == "Ordine alfabetico":
        dict_words = OrderedDict(sorted(dict_words.items()))
    elif ordine == "Ordine alfabetico inverso":
        dict_words = OrderedDict(sorted(dict_words.items(), reverse=True))

    return dict_words
