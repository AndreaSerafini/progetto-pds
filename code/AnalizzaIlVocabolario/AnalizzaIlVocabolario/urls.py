"""AnalizzaIlVocabolario URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib.auth.views import LoginView, LogoutView
from django.urls import path, reverse_lazy
from django.contrib.auth import views as auth_views
from . import views

urlpatterns = [
    path('', views.Home, name='homepage'),
    path('visualizza', views.VisualizzaParole, name='visualizza'),
    path('inserisci', views.InserisciTesto, name='inserisci'),
    # path('login', views.Login, name='login'),
    path('ricerca', views.ricercaTesti, name='ricerca'),
    # url(r'^register/$', views.register, name='register'),
    path('blacklist', views.Blacklist, name='blacklist'),
    path('confronto', views.Confronto, name='confronto'),
    path('login', LoginView.as_view(), name='login'),
    path('logout', LogoutView.as_view(next_page='/'), name='logout'),
    path('ricerca', views.ricercaTesti, name='ricerca'),
    path('register', views.register, name='register'),
    path('grafico', views.grafico, name='grafico'),
    path('legacy', views.legacy, name='legacy'),
    path('activate/<slug:uidb64>/<slug:token>/', views.activate_account, name='activate'),
    path('carica',views.CaricaParole,name ='carica'),
    path('traduci',views.traduci,name='traduci'),

    path('password-reset/',
         auth_views.PasswordResetView.as_view(
             template_name='registration/password_reset_form.html',
             subject_template_name='registration/password_reset_subject.txt',
             success_url=reverse_lazy('password_reset_done')
         ),
         name='password_reset'),

    path('password-reset/done/',
         auth_views.PasswordResetDoneView.as_view(
             template_name='registration/password_reset_done.html'
         ),
         name='password_reset_done'),

    path('password-reset-confirm/<uidb64>/<token>/',
         auth_views.PasswordResetConfirmView.as_view(
             template_name='registration/password_reset_confirm.html'
         ),
         name='password_reset_confirm'),

    path('password-reset-complete/',
         auth_views.PasswordResetCompleteView.as_view(
             template_name='registration/password_reset_complete.html'
         ),
         name='password_reset_complete'),

]
