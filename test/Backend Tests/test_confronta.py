import os
import django
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'AnalizzaIlVocabolario.settings')
django.setup()

from Backend.confronta import unisci_testi, indici_gulpease


def test_unisci_testi():
    soluzione_corretta = {'è': [('Test', 2), ('Test2', 1)], 'test': [('Test', 1), ('Test2', 1)],
                          'secondo': [('Test', 0), ('Test2', 1)], 'che': [('Test', 1), ('Test2', 0)],
                          'il': [('Test', 3), ('Test2', 1)], 'incompleto': [('Test', 1), ('Test2', 0)], 'per':
                              [('Test', 1), ('Test2', 0)], 'utilizzeremo': [('Test', 1), ('Test2', 0)],
                          'testo': [('Test', 2), ('Test2', 0)], 'questo': [('Test', 1), ('Test2', 1)]}

    assert soluzione_corretta == unisci_testi(['Test', 'Test2'])


def test_indici_gulpease():
    soluzione_corretta = [('Test', 69, 'andreas'), ('Test2', 100, 'mirkoc')]

    assert soluzione_corretta == indici_gulpease(['Test', 'Test2'])
